# Make your UI rounder with Vircle!

This plugin for Godot 4 adds a Container _Vircle_ to the Engine. It alligns all its children in a circle, but it can also do some other things:

- Adjust the size of all children elements at once
- Adjust radius of the circle
- Rotate elements around one another automatically (great for visuals)
